# ipv6y

`ipv6y` is a user space callback for `iptables` which maps outgoing connections
to different IPv6 addresses of a single interface. This can be used to anonymize
network traffic if the user has multiple IPv6 addresses or prefixes available.

**Warning:** This can only be used for network level anonymization and does not
protect against other tracking mechanisms such as cookies, fingerprinting, etc.

This is a proof of concept implementation and should be considered experimental!

`ipv6y` relies on network address translation (NAT) to rewrite IP addresses of
outgoing and incoming packets to masquerade the origin. Only TCP connections are
tracked right now. For every other protocol each packet is masqueraded
separately which might cause problems, if multiple packets are transmitted. So
better just block everything that is not TCP until other connections are
handled.

## Prerequisites

* Linux Kernel version 3.7 or higher, configured with IPv6 NAT Support (see
  **Linux Kernel Configuration** section below)
* [python-nfqueue](https://github.com/blochberger/python-nfqueue)
  * `cmake`
  * `libnetfilter-nfqueue`
  * `swig`
  * Python development files
    * `pip install dpkt` (See [dpkt](https://github.com/kbandla/dpkt),
      [Documentation](https://dpkt.readthedocs.org/en/latest/index.html))
    * `pip install netifaces`

### Linux Kernel Configuration

A kernel version 3.7+ is required according to
[`man ip6tables`](http://ipset.netfilter.org/ip6tables.man.html)

To enable IPv6 NAT and NFQUEUE support you need to build your kernel with:

```
[*] Networking Support  --->
    Networking options  --->
        [*] Network packet filtering framework (Netfilter)  --->
            Core Netfilter Configuration  --->
                <M> IPv4/IPv6 redirect support
                {M} Netfilter Xtables support (required for ip_tables)
                <M>   "NFQUEUE" target Support
                -M-   "SNAT and DNAT" targets support
            IPv6: Netfilter Configuration  --->
                -M- IPv6 NAT
                -M-   IPv6 masquerade support
                <M> ip6tables NAT support
                <M>   MASQUERADE target support
                <M>   NPT (Network Prefix transllation) target support
```

You can test if IPv6 NAT support is enabled in your current kernel by executing:

```sh
ip6tables --table nat --list
```

If IPv6 NAT support is not enabled by your kernel `ip6tables` will exit with a
non-zero exit code (3) and will display the following error message:

```
modprobe: ERROR: could not insert 'ip6_tables': Invalid argument
ip6tables v1.4.21: can't initialize ip6tables table `nat': Table does not exist (do you need to insmod?)
Perhaps ip6tables or your kernel needs to be upgraded.
```

## How to run

First you need to setup all prerequisites as described in the **Prerequisites**
section.

You need root permissions to perform the following steps.

Assuming `eth0` is your private (fe. LAN) interface and `eth1` is your public
(fe. WAN) interface where you want to anonymize your outgoing network traffic.

You need to setup a queue which can be processed by a user space application
such as `ipv6y`. We are interested to process outgoing connections from your
private network to the public network. This can be done by adding the following
rule to your `ip6tables` configuration:

```sh
ip6tables --append FORWARD --in-interface eth0 --out-interface eth1 --jump NFQUEUE
```

or short

```sh
ip6tables -A FORWARD -i eth0 -o eth1 -j NFQUEUE
```

Then you need to start the `ipv6y` application which registers to the queue and
processes it. (Right now the queue ID needs to be changed in the `ipv6y` file
directly if you use multiple queues.) You can do this by running:

```sh
ipv6y eth1
```

Remember `eth1` is the public interface. It should have multiple IPv6 addresses
assigned, so that they can be used to anonymize your traffic.

You can show the IPv6 addresses of the `eth1` interface by executing:

```sh
ip -6 addr show eth1 | grep global
```

**Warning:** Note that when starting `ipv6y` your IPv6 `nat` table will be
flushed. To avoid loosing your rules you should save your configuration:

```sh
ip6tables-save -t nat > ip6tables-nat-backup
```

This will save your current ruleset of the `nat` table to a file named
`ip6tables-nat-backup` which you can restore by issuing:

```sh
ip6tables-restore < ip6tables-nat-backup
```

### Static IP addresses

If you like to exclude IPv6 addresses from mapping, fe. if you have a server
running and want to be reachable via a static IPv6 address then you probably
don't want to expose this address to the public network for your outgoing
traffic (except for your servers replies of course) then you can simply add
these addresses after the argument where you specify your public interface.
Fe. if your static addresses should be `2001:db8::1` and `2001:db8::2` you would
call:

```sh
ipv6y eth1 2001:db8::1 2001:db8::2
```

## Basic Idea

This is a quick introduction to `ip6tables` and what's happening in the
background when `ipv6y` is running.

`ipv6y` adds SNAT and DNAT rules if it detects an unknown connection. Only TCP
connections are handled right now. If it detects a FIN flag in a TCP packet the
SNAT and DNAT rules are queued for removal and will be removed after the packet
containing the FIN flag has been processed. If a new TCP connection is
established for a known connection it is assumed that the existing connection
got separated without seeing a FIN packet -- which might be the case if an error
had occurred.

To view the content of the `nat` table:

```sh
ip6tables --table nat --list POSTROUTING --verbose
```

To start anew you might want to flush the `nat` table by issuing:

```sh
ip6tables --table nat --flush
```

Then we replace the source address of outgoing packets and replace it with one
address of our bucket, fe. `2001:db8::1/34` if the destination address is fe.
`2001:db8::2/34` using the TCP protocol:

```sh
ip6tables --table nat --append POSTROUTING --protocol tcp --destination 2001:db8::2 --jump SNAT --to-source 2001:db8::1
```

To remove the previous rule again:

```sh
ip6tables --table nat --delete POSTROUTING --protocol tcp --destination 2001:db8::2 --jump SNAT --to-source 2001:db8::1
```

## Simple Test Setup

Simply execute `testserver` on your server and open it in a browser/client.

Alternatively you can use PHP, if you already have a web server with PHP running, you can add the following PHP script, which shows the IP address used by the client:

```php
<?php
header( 'Content-Type: text/plain' );
print( $_SERVER[ 'REMOTE_ADDR' ] );
?>
```

The PHP version is more reliable since the `testserver` sometimes crashes if you
are experimenting with NAT rules and provide multiple IP addresses for a single
TCP connection. This smells like a bug in the Python module used for the
`testserver` implementation. You might miss the crash if you don't have the
output of the `testserver` on your screen.

## Debugging `ip6tables`
See [iptables debugging](http://backreference.org/2010/06/11/iptables-debugging/)
