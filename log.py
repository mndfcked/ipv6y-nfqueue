# -*- coding: utf-8 -*-

import sys

# Color terminal output if termcolor package is available. It can be installed
# by running: pip install termcolor
try:
    from termcolor import colored
except ImportError:
    def colored(str, color):
        return str


def info(msg):
    print(colored(msg, 'magenta'))


def warning(msg):
    print(colored(msg, 'yellow'))


def error(msg):
    print >> sys.stderr, colored(msg, 'red')
